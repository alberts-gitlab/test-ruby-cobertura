require 'spec_helper'

RSpec.describe 'Demo' do
  subject { Demo.new }
  
  describe '#test' do
    it { expect(subject.test).to eq('test') }

    it { expect(subject.bar).to eq('bar') }
  end
end
